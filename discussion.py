
# username = input("Please enter your username: \n")
# print(f"Hello {username}! Welcome to the Python short course!")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")
# The output is the concatenation of num1 and num2 values. The reason for this is the input( method assigns any values as STRINGS)

# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))
# print(f"The sum of num1 and num2 is {num1 + num2}")


# [Section] If - Else Statement
test_num = 75

if test_num >= 60:
	print("Test passed")
	print("Congratulations")
else:
	print("Test failed")
# Note that in Python, curly braces are not needed to distinguish the code blocks inside the if or else block. Hence, indentions are very important as Python uses indentions to distinguish parts of code as needed.


# [Section] If Else Chains can be also used to have more than 2 choices
# test_num2 = int(input("Please enter the 2nd test number: \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")
# Note: elif is the shorthand for "else if" in other programming languages.

# test_num3 = int(input("Please enter the 3rd test number: \n"))

# if test_num3 % 3 == 0 and test_num3 % 5 == 0:
# 	print(f"{test_num3} is divisible by both 3 and 5.")
# elif test_num3 % 3 == 0:
# 	print(f"{test_num3} is divisible by 3.")
# elif test_num3 % 5 == 0:
# 	print(f"{test_num3} is divisible by 5.")
# else:
# 	print(f"{test_num3} is not divisible by 3 nor 5.")


# [Section] Loops

# Python has loops that can repeat blocks of code

# >> While Loop are used to execute a set of statement as long as the condition is true
i = 1
while i <= 5: #as long as our i is lesser or equal to 5, the loop will run
	print(f"Current count {i}")
	i += 1 # increment by 1 every after the while condition is satisfied

# >> For Loops are used for iterating a sequence
fruits = ["apple", "banana", "cherry"]
	# Variable for each fruit
for each_fruit in fruits: # fruits array
	print(each_fruit)

print("--------")
# [Section] range() method
# To use the for loop to iterate through values, the range method can be used
for x in range(6):
	print(f"The current value is {x}")
# The range() function defaults to 0 as starting value. As such, this prints from 0 to 5

print("----------")
for x in range(1,6):
	print(f"The current value is {x}")
# starting value is 1

print("-----------")
for x in range(0,10,2):
	print(f"The current value is {x}")
# incerements by 2, starting value of 0


# [Section] Break Statement
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j += 1

print("----------")
# [Section] Continue Statement
# The continue statement returns the control to the beginning of the while loop and continue the next iteration
z = 1
while z < 6:
	z += 1
	if z == 3:
		continue # returns back to the loop condition
	print(f"Current value after incrementation {z}")


print("------------")
# k = 0
# while k < 6:
# 	if k == 3:
# 		continue # skips othere statements
# 	k += 1
# 	print(f"Current value after incrementation {k}")
# 	